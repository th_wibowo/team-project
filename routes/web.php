<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('admin');

Route::get('/template', function () {
    return view('teamproject.template.master');
});

Auth::routes();

Route::get('/home', 'HomeController@admin')->name('home');

Route::get('/pilihkos', 'PilihKosController@pilih')->name('pilihkos');

Route::get('/pilihkos/{kos_id}','PilihKosController@proses');

//Route::get('/pilihkos/{kos_id}/edit','PilihKosController@checkout');

Route::group(['middleware' => ['auth', 'ceklevel:admin']], function() {
    Route::resource('pemilik', 'PemilikController');
    Route::resource('kos', 'KosController');
});

Route::group(['middleware' => ['auth', 'ceklevel:admin,user']], function() {
    Route::resource('penyewa', 'PenyewaController');
    Route::resource('transaksi', 'SewaController');
});