<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaksi_Sewa extends Model
{
  protected $table = "transaksi_sewa";
  protected $fillable = ["tgl_sewa", "jenis_sewa", "biaya", "kos_id", "penyewa_id"];
  //protected $guarded = [];

  public function kos(){
    return $this->belongsTo('App\Kos', 'kos_id');
  }

  public function penyewa(){
    return $this->belongsTo('App\Penyewa', 'penyewa_id');
  }

  public function pemilik(){
    return $this->belongsToMany('App\Pemilik', 'laporan_transaksi_sewa', 'pemilik_id', 'transaksi_sewa_id');
    //('Source Model', 'nama tabel1_has_tabel2', 'foreign_key1', 'foreign_key2')
  }
}
