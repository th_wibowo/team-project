<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Laporan_Transaksi_Sewa extends Model
{
  protected $table = "laporan_transaksi_sewa";
  protected $guarded = [];
}
