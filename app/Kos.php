<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kos extends Model
{
  protected $table = "kos";
  protected $fillable = ["name", "alamat", "jenis_kos", "fasilitas", "harga", "stok_kamar","pemilik_id"];
  //protected $guarded = [];

  public function pemilik(){
    return $this->belongsTo('App\Pemilik', 'pemilik_id');
  }

  public function transaksi_sewa(){
    return $this->hasMany('App\Transaksi_Sewa');
  }
}
