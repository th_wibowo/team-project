<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Penyewa extends Model
{
  protected $table = "penyewa";
  protected $fillable = ["name", "jenis_kelamin", "alamat", "email", "no_hp", "no_ktp", "pekerjaan", "foto", "users_id"];
  //protected $guarded = [];

  public function user(){
    return $this->belongsTo('App\User', 'user_id');
  }

  public function transaksi_sewa(){
    return $this->hasOne('App\Transaksi_Sewa');
  }
}
