<?php

namespace App\Http\Controllers;

use App\Pemilik;
Use Auth;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class PemilikController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pemilik = Pemilik::all();
        return view('teamproject.pemilik.index', compact('pemilik'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('teamproject.pemilik.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'          => 'required',
            'jenis_kelamin' => 'required',
            'alamat'        => 'required|max:255',
            'email'         => 'required|unique:pemilik|email',
            'no_rekening'   => 'required'
        ]);

        $pemilik = Pemilik::create([
            "name"          => $request["name"],
            "jenis_kelamin" => $request["jenis_kelamin"],
            "alamat"        => $request["alamat"],
            "email"         => $request["email"],
            "no_rekening"   => $request["no_rekening"],
            "users_id"      => Auth::id()
        ]);

        Alert::success('Success', 'Berhasil Menambahkan Data Pemilik Kos Baru');

        return redirect('/pemilik')->with('success', 'Post Berhasil Disimpan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pemilik = Pemilik::find($id);
        return view('teamproject.pemilik.show', compact('pemilik'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pemilik = Pemilik::find($id);
        return view('teamproject.pemilik.edit', compact('pemilik'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update = Pemilik::where('id', $id)->update([
            "name"          => $request["name"],
            "jenis_kelamin" => $request["jenis_kelamin"],
            "alamat"        => $request["alamat"],
            "email"         => $request["email"],
            "no_rekening"   => $request["no_rekening"]
        ]);
        
        Alert::success('Success', 'Berhasil Update Data Pemilik Kos');

        return redirect('/pemilik')->with('success', 'Berhasil Update Pemilik!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Pemilik::destroy($id);
        return redirect('/pemilik')->with('success', 'Pemilik Berhasil Dihapus');
    }
}
