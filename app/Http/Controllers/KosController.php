<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
use App\Kos;
use App\Pemilik;
Use Auth;

class KosController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kos = Kos::all();
        return view('teamproject.kos.index', compact('kos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pemilik = Pemilik::all();
        //dd($pemilik);
        foreach($pemilik as $value => $owner){
          if ($owner = Auth::id()){
            $pemilikyanglogin = Pemilik::find($owner);
          }
        }

        return view('teamproject.kos.create', compact('pemilikyanglogin'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'          => 'required|unique:kos|max:255',
            'alamat'        => 'required|max:255',
            'jenis_kos'     => 'required',
            'fasilitas'     => 'required|max:255',
            'harga'         => 'required',
            'stok_kamar'    => 'required'
        ]);

        $kos = Kos::create([
            "name"          => $request["name"],
            "alamat"        => $request["alamat"],
            "jenis_kos"     => $request["jenis_kos"],
            "fasilitas"     => $request["fasilitas"],
            "harga"         => $request["harga"],
            "stok_kamar"    => $request["stok_kamar"],
            "pemilik_id"    => $request["pemilik_id"]
        ]);

        Alert::success('Success', 'Berhasil Menambahkan Data Kos Baru');

        return redirect('/kos')->with('success', 'Post Berhasil Disimpan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $kos = Kos::find($id);
        return view('teamproject.kos.show', compact('kos'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kos = Kos::find($id);
        return view('teamproject.kos.edit', compact('kos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update = Kos::where('id', $id)->update([
            "name"          => $request["name"],
            "alamat"        => $request["alamat"],
            "jenis_kos"     => $request["jenis_kos"],
            "fasilitas"     => $request["fasilitas"],
            "harga"         => $request["harga"],
            "stok_kamar"    => $request["stok_kamar"]
        ]);

        Alert::success('Success', 'Berhasil Update Data Kos');

        return redirect('/kos')->with('success', 'Berhasil Update Kos!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Kos::destroy($id);
        return redirect('/kos')->with('success', 'Kos Berhasil Dihapus');
    }
}
