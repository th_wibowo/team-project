<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Transaksi_Sewa;
use App\Kos;
use App\User;
use App\Penyewa;
use Auth;

class PilihKosController extends Controller
{
    public function pilih(){
      $kos = Kos::all();
      return view('teamproject.transaksi.pilihkos', compact('kos'));
    }

    public function proses($id){
        //mengambil auth id dan memasukkan kedalam query pencarian
        $kosan = Kos::find($id);
        $authid = Auth::id();
        $user = User::find($authid);
        $penyewa = Penyewa::where('name', $user->name)->get();//SELECT * FROM penyewa WHERE name = $user->name;
        //dd($penyewa);
        return view('teamproject.transaksi.create', compact('kosan', 'penyewa'));

    }
}
