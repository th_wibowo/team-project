<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
use App\Transaksi_Sewa;
use App\Kos;
Use Auth;
USe PDF;

class SewaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sewa = Transaksi_Sewa::all();
        return view('teamproject.transaksi.index', compact('sewa'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('teamproject.transaksi.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'tgl_sewa'      => 'required',
            'jenis_sewa'    => 'required',
            'biaya'         => 'required|integer',
            'kos_id'        => 'required',
            'penyewa_id'    => 'required'
        ]);
        //dd($request);
        $sewa = Transaksi_Sewa::create([
            "tgl_sewa"      => $request["tgl_sewa"],
            "jenis_sewa"    => $request["jenis_sewa"],
            "biaya"         => $request["biaya"],
            "penyewa_id"    => $request["penyewa_id"],
            "kos_id"        => $request["kos_id"]
        ]);

        Alert::success('Success', 'Berhasil Menambahkan Data Sewa Kos Baru');

        return redirect('/transaksi');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $sewa = Transaksi_Sewa::find($id);

        //memasukkan ke dalam dompdf
        $pdf = PDF::loadView('teamproject.pdf.invoice', compact('sewa'));
        return $pdf->download('invoicedetail.pdf');

        //return view('teamproject.transaksi.show', compact('sewa'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sewa = Transaksi_Sewa::find($id);
        return view('teamproject.transaksi.edit', compact('sewa'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update = Transaksi_Sewa::where('id', $id)->update([
            "tgl_sewa"      => $request["tgl_sewa"],
            "jenis_sewa"    => $request["jenis_sewa"],
            "biaya"         => $request["biaya"]
        ]);

        Alert::success('Success', 'Berhasil Update Data Sewa Kos');

        return redirect('/transaksi')->with('success', 'Berhasil Update Transaksi Sewa!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Transaksi_Sewa::destroy($id);
        return redirect('/transaksi')->with('success', 'Pemilik Berhasil Dihapus');
    }
}
