<?php

namespace App\Http\Controllers;

use App\Penyewa;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
use Auth;

class PenyewaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth')->except(['index','create']);
    }

    public function index()
    {
        $penyewa = Penyewa::all();
        return view('teamproject.penyewa.index', compact('penyewa'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('teamproject.penyewa.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'          => 'required',
            'jenis_kelamin' => 'required',
            'alamat'        => 'required|max:255',
            'email'         => 'required|unique:pemilik|email',
            'no_hp'         => 'required',
            'no_ktp'        => 'required',
            'pekerjaan'     => 'required'
        ]);

        if($request->hasFile('foto'))
        {
            //upload foto
            $file               = $request->file('foto');
            $fileName           = $time() . '-' .$file->getClientOriginalName();
            $file -> move(public_path('uploads'), $fileName);

        $penyewa = Penyewa::create([
            "name"          => $request["name"],
            "jenis_kelamin" => $request["jenis_kelamin"],
            "alamat"        => $request["alamat"],
            "email"         => $request["email"],
            "no_hp"         => $request["no_hp"],
            "no_ktp"        => $request["no_ktp"],
            "pekerjaan"     => $request["pekerjaan"],
            'foto'          => $fileName,
            "users_id"      => Auth::id()

        ]);
        }else
        {
            $penyewa = Penyewa::create([
                "name"          => $request["name"],
                "jenis_kelamin" => $request["jenis_kelamin"],
                "alamat"        => $request["alamat"],
                "email"         => $request["email"],
                "no_hp"         => $request["no_hp"],
                "no_ktp"        => $request["no_ktp"],
                "pekerjaan"     => $request["pekerjaan"],
                "users_id"      => Auth::id()
            ]);
        }

        Alert::success('Success', 'Berhasil Menambahkan Data Penyewa Kos Baru');

        return redirect('/penyewa')->with('success', 'Post Berhasil Disimpan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $penyewa = Penyewa::find($id);
        return view('teamproject.penyewa.show', compact('penyewa'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $penyewa = Penyewa::find($id);
        return view('teamproject.penyewa.edit', compact('penyewa'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update = Penyewa::where('id', $id)->update([
            "name"              => $request["name"],
            "jenis_kelamin"     => $request["jenis_kelamin"],
            "alamat"            => $request["alamat"],
            "email"             => $request["email"],
            "no_hp"             => $request["no_hp"],
            "no_ktp"            => $request["no_ktp"],
            "pekerjaan"         => $request["pekerjaan"]
        ]);

        Alert::success('Success', 'Berhasil Update Data Penyewa Kos');

        return redirect('/penyewa')->with('success', 'Berhasil Update Penyewa!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Penyewa::destroy($id);
        return redirect('/penyewa')->with('success', 'Penyewa Berhasil Dihapus');
    }
}
