<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pemilik extends Model
{
  protected $table = "pemilik";
  protected $guarded = [];

  public function user(){
    return $this->belongsTo('App\User', 'user_id');
  }

  public function kos(){
    return $this->hasMany('App\Kos');
    //atau return $this->hasOne('App\Pemilik', 'nama_foreign_key');
  }

  public function transaksi_sewa(){
    return $this->belongsToMany('App\Transaksi_Sewa', 'laporan_transaksi_sewa', 'pemilik_id', 'transaksi_sewa_id');
    //('Source Model', 'nama tabel1_has_tabel2', 'foreign_key1', 'foreign_key2')
  }
}
