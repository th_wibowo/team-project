@extends('teamproject.template.adminpanel.adminmaster')

@section('content')
<div class="ml-3 mr-3">
    <div class="card card-primary">
        <div class="card-header">
        <h3 class="card-title">Create New Post Kos</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form action="/pemilik" method="POST">
        @csrf
        <div class="card-body">
            <div class="form-group">
                <label for="name">Nama Pemilik</label>
                <input type="text" class="form-control" id="name" name="name" value=" {{ old('name', '') }}" placeholder="Enter Nama Pemilik">
                @error('name')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="exampleFormControlSelect1">Jenis Kelamin</label>
                <select name="jenis_kelamin" class="form-control" id="exampleFormControlSelect1">
                <option value="L">Laki-Laki</option>
                <option value="P">Perempuan</option>
                </select>
                @error('jenis_kelamin')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="alamat">Alamat</label>
                <input type="text" class="form-control" id="alamat" name="alamat" value=" {{ old('alamat', '') }}" placeholder="Enter Alamat">
                @error('alamat')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="email">Email</label>
                <input type="email" class="form-control" id="email" name="email" value=" {{ old('email', '') }}" placeholder="Enter Email">
                @error('email')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="no_rekening">No Rekening</label>
                <input type="text" class="form-control" id="no_rekening" name="no_rekening" value=" {{ old('no_rekening', '') }}" placeholder="Enter No Rekening">
                @error('no_rekening')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
        </div>
        <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Create</button>
        </div>
        </form>
    </div>
</div>
@endsection