@extends('teamproject.template.adminpanel.adminmaster')

@section('content')

<div class="row">
    <div class="col-md-6">
        <div class="card ml-3 " style="width: 25rem;">
        <img class="card-img-top" src="#" alt="Card image cap">
        <div class="card-body">
            <h3 class="card-title">Nama pemilik: {{ $pemilik->name }} </h3>
            <p class="card-text">Jenis kelamin: {{ $pemilik->jenis_kelamin }}</p>
            <p>Alamat: {{ $pemilik->alamat }}</p>
            <p class="card-text">Email: {{ $pemilik->email }} </p>
            <p class="card-text">No. Rekening: {{ $pemilik->no_rekening }} </p>
        </div>
        </div>
    </div>
</div>  
</div>
   
@endsection
