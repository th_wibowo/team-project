@extends('teamproject.template.adminpanel.adminmaster')

@section('content')
<div class="ml-3 mr-3">
    <div class="card card-primary">
        <div class="card-header">
        <h3 class="card-title">Create New Sewa</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form action="/transaksi" method="POST">
        @csrf
        <div class="card-body">
            <div class="form-group">
                <label for="tgl_sewa">Tanggal Sewa</label>
                <input type="date" class="form-control" id="tgl_sewa" name="tgl_sewa" value=" {{ old('tgl_sewa', '') }}" placeholder="Enter Tanggal Sewa">
                @error('tgl_sewa')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="exampleFormControlSelect1">Kos Yang Dipilih</label>
                <select name="kos_id" class="form-control" id="exampleFormControlSelect1">
                    <option value="{{$kosan->id}}">{{$kosan->name}}</option>
                </select>
            </div>
            <div class="form-group">
                <label for="exampleFormControlSelect1">Jenis Sewa</label>
                <select name="jenis_sewa" class="form-control" id="exampleFormControlSelect1">
                    <option value="M">PerMinggu</option>
                    <option value="B">PerBulan</option>
                    <option value="T">PerTahun</option>
                </select>
                @error('jenis_sewa')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="biaya">Biaya</label>
                <input type="text" class="form-control" id="biaya" name="biaya" value=" {{ old('biaya', '') }}" placeholder="Enter Biaya">
                @error('biaya')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            @foreach($penyewa as $object)<!-- atau dengan {{ $penyewa->first() }} , yaitu mengambil properti pertama dari koleksi $penyewa, karena $penyewa hanya berisi 1 object array-->
                  <input type="hidden" name="penyewa_id" value="{{ $object->id }}">
            @endforeach

        </div>
        </div>
        <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Create</button>
        </div>
        </form>
    </div>
</div>
@endsection

<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script>
    $('#jenis_sewa').on('change', function() {
    $('.input_harga').val($(this).val());
    });
</script> -->
