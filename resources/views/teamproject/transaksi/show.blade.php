@extends('teamproject.template.adminpanel.adminmaster')

@section('content')
    <div class="mt-3 ml-3">
        <h4>Tanggal mulai sewa kos: {{ $sewa->tgl_sewa }} </h4>
        <h4>Jenis sewa kos: {{ $sewa->jenis_sewa }} </h4>
        <h4>Total biaya: {{ $sewa->biaya }} </h4>
        <h4>Nama Penyewa: {{ $sewa->penyewa_id }} </h4>
        <h4>Nama Kos: {{ $sewa->kos_id }} </h4>
    </div>
@endsection
