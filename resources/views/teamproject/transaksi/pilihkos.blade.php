@extends('teamproject.template.adminpanel.adminmaster')

@push('style')
<link rel="stylesheet" href="{{('adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
@endpush

@section('content')
<div class="mt-3 mr-3">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">List Kos</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            @if(session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
            <table id="example1" class="table table-bordered">
            <thead>
                <tr>
                <th style="width: 10px">#</th>
                <th>Nama Kos</th>
                <th>alamat</th>
                <th>Jenis Kos</th>
                <th>Fasilitas</th>
                <th>Harga</th>
                <th>Stok Kamar</th>
                <th style="width: 40px">Actions</th>
                </tr>
            </thead>
            <tbody>
                @forelse($kos as $value => $kosan)
                    <tr>
                        <td> {{ $value + 1 }} </td>
                        <td> {{ $kosan->name }} </td>
                        <td> {{ $kosan->alamat }} </td>
                        <td> {{ $kosan->jenis_kos }} </td>
                        <td> {{ $kosan->fasilitas }} </td>
                        <td> {{ $kosan->harga }} </td>
                        <td> {{ $kosan->stok_kamar }} </td>
                        <td style="display: flex;" class="justify-content-around">
                            <a href="/pilihkos/{{$kosan->id}}" class="btn btn-default btn-sm">Pilih</a>
                        </td>
                    </tr>
                    @empty
                        <tr>
                            <td colspan="8" align="center">Tidak ada kosan tersedia</td>
                        </tr>
                @endforelse
            </tbody>
            </table>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script src="{{ asset('adminlte/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{ asset('adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>
@endpush
