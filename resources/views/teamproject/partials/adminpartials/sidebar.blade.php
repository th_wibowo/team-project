<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="../../index3.html" class="brand-link">
      <img src="{{ asset('adminlte/dist/img/AdminLTELogo.png')}}"
           alt="AdminLTE Logo"
           class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">Aplikasi Sewa Kos</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{ asset('adminlte/dist/img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">{{ Auth::user()->name }}</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="{{ route('home') }}" class="nav-link active">
              <i class="nav-icon fas fa-home"></i>
              <p>
                Home
              </p>
            </a>
          </li>
          @auth
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-table"></i>
              <p>
                Manajemen Data
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            @if (auth()->user()->role_id=="admin")
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="/pemilik" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Data Pemilik</p>
                </a>
              </li>
            </ul>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="/kos" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Data Kos</p>
                </a>
              </li>
            </ul>
            @endif
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="/penyewa" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Data Penyewa</p>
                </a>
              </li>
            </ul>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="/transaksi" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Transaksi Sewa</p>
                </a>
              </li>
            </ul>
            <div class="mt-3">
              <form action="{{ route('logout') }}" method="post">
                @csrf
                <button type="submit" class="btn btn-block btn-outline-danger btn-sm">Logout</button>
              </form>
            </div>
          </li>
          @endauth
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>