<nav class="header-nav">
  <ul class="main-menu">
    <li><a href="index.html" class="active">Home</a></li>
    <li><a href="about-us.html">About</a></li>
    <li><a href="#">Buy</a></li>
    <li><a href="#">Pages</a>
      <ul class="sub-menu">
        <li><a href="about-us.html">About Us</a></li>
        <li><a href="search-result.html">Search Result</a></li>
        <li><a href="single-property.html">Property</a></li>
      </ul>
    </li>
    <li><a href="news.html">News</a></li>
    <li><a href="contact.html">Contact</a></li>
  </ul>
  <div class="header-right">
    <div class="user-panel">
      <a href="#" class="login">Sign in</a>
      <a href="#" class="register">Join us</a>
    </div>
  </div>
</nav>
