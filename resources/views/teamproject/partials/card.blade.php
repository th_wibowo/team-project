<section class="property-section">
  <div class="container">
    <div class="section-title">
      <h2>Recently Added Rumah Kos</h2>
    </div>
    <div class="row">
      <div class="col-lg-4">
        <div class="property-item">
          <div class="pi-image">
            <img src="{{ asset('/teamproject/img/property/1.jpg') }}" alt="">
            <div class="pi-badge new">New</div>
          </div>
          <h3>$469,000</h3>
          <h5>3 Bedrooms Townhouse</h5>
          <p>Donec eget efficitur ex. Donec eget dolor vitae eros feugiat tristique id vitae massa. Proin vulputate congue rutrum. Fusce lobortis a enim eget tempus. Class aptent taciti sociosqu ad litora.</p>
          <a href="#" class="readmore-btn">Lebih lanjut..</a>
        </div>
      </div>
      <div class="col-lg-4">
        <div class="property-item">
          <div class="pi-image">
            <img src="{{ asset('/teamproject/img/property/2.jpg') }}" alt="">
            <div class="pi-badge offer">Offer</div>
          </div>
          <h3>$369,000</h3>
          <h5>3 Bedrooms Townhouse</h5>
          <p>Donec eget efficitur ex. Donec eget dolor vitae eros feugiat tristique id vitae massa. Proin vulputate congue rutrum. Fusce lobortis a enim eget tempus. Class aptent taciti sociosqu ad litora.</p>
          <a href="#" class="readmore-btn">Lebih lanjut..</a>
        </div>
      </div>
      <div class="col-lg-4">
        <div class="property-item">
          <div class="pi-image">
            <img src="{{ asset('/teamproject/img/property/3.jpg') }}" alt="">
            <div class="pi-badge new">New</div>
          </div>
          <h3>$560,000</h3>
          <h5>3 Bedrooms Townhouse</h5>
          <p>Donec eget efficitur ex. Donec eget dolor vitae eros feugiat tristique id vitae massa. Proin vulputate congue rutrum. Fusce lobortis a enim eget tempus. Class aptent taciti sociosqu ad litora.</p>
          <a href="#" class="readmore-btn">Lebih lanjut..</a>
        </div>
      </div>
    </div>
  </div>
</section>
