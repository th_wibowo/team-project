<section class="intro-section spad">
  <div class="container">
    <div class="section-title">
      <h2>Best Dealer Rumah Kos Se-Indonesia</h2>
    </div>
    <div class="row intro-first">
      <div class="col-lg-6 order-lg-2">
        <img src="{{ asset('/teamproject/img/about/1.jpg') }}" alt="">
      </div>
      <div class="col-lg-6 order-lg-1">
        <div class="about-text">
          <h3>We charge 2% total. No hidden fees or upfront costs.</h3>
          <p>Donec eget efficitur ex. Donec eget dolor vitae eros feugiat tristique id vitae massa. Proin vulputate congue rutrum. Fusce lobortis a enim eget tempus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Suspendisse potenti. Ut gravida mattis magna, non varius lorem sodales nec. In libero orci, ornare non nisl a, auctor euismod purus. Morbi pretium interdum vestibulum. Fusce nec eleifend ipsum. Sed non blandit tellus.</p>
          <a href="#" class="readmore-btn">Lebih lanjut..</a>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-6">
        <img src="img/about/2.jpg" alt="">
      </div>
      <div class="col-lg-6 ">
        <div class="about-text">
          <h3>How Much Can You Save? We work for you, not commission.</h3>
          <p>Donec eget efficitur ex. Donec eget dolor vitae eros feugiat tristique id vitae massa. Proin vulputate congue rutrum. Fusce lobortis a enim eget tempus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Suspendisse potenti. Ut gravida mattis magna, non varius lorem sodales nec. In libero orci, ornare non nisl a, auctor euismod purus. Morbi pretium interdum vestibulum. Fusce nec eleifend ipsum. Sed non blandit tellus.</p>
          <a href="#" class="readmore-btn">Lebih lanjut..</a>
        </div>
      </div>
    </div>
  </div>
</section>
