@extends('teamproject.template.adminpanel.adminmaster')

@section('content')
<div class="ml-3 mr-3">
    <div class="card card-primary">
        <div class="card-header">
        <h3 class="card-title">Create New Post Kos</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form action="/kos" method="POST">
        @csrf
        <div class="card-body">
            <div class="form-group">
                <label for="name">Nama Kos</label>
                <input type="text" class="form-control" id="name" name="name" value=" {{ old('name', '') }}" placeholder="Enter Nama Koss">
                @error('name')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="exampleFormControlSelect1">Pemilik Kos</label>
                <select name="pemilik_id" class="form-control" id="exampleFormControlSelect1">
                    <option value="{{ $pemilikyanglogin->id }}">{{ $pemilikyanglogin->name}}</option>
                </select>
            </div>
            <div class="form-group">
                <label for="alamat">Alamat</label>
                <input type="text" class="form-control" id="alamat" name="alamat" value=" {{ old('alamat', '') }}" placeholder="Enter Alamat">
                @error('alamat')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="exampleFormControlSelect1">Jenis Kos</label>
                <select name="jenis_kos" class="form-control" id="exampleFormControlSelect1">
                <option value="L">Laki-Laki</option>
                <option value="P">Perempuan</option>
                </select>
                @error('jenis_kos')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="fasilitas">Fasilitas</label>
                <input type="text" class="form-control" id="fasilitas" name="fasilitas" value=" {{ old('fasilitas', '') }}" placeholder="Enter Fasilitas">
                @error('fasilitas')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="harga">Harga</label>
                <input type="text" class="form-control" id="harga" name="harga" value=" {{ old('harga', '') }}" placeholder="Enter Harga Kos">
                @error('harga')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="stok_kamar">Stok Kamar</label>
                <input type="text" class="form-control" id="stok_kamar" name="stok_kamar" value=" {{ old('stok_kamar', '') }}" placeholder="Enter dalam satuan">
                @error('stok_kamar')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
        </div>
        <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Create</button>
        </div>
        </form>
    </div>
</div>
@endsection
