@extends('teamproject.template.adminpanel.adminmaster')

@section('content')
    <div class="mt-3 ml-3">
        <h4>Nama Kos : {{ $kos->name }}</h4>
        <h4>Alamat: {{ $kos->alamat }} </h4>
        <h4>Jenis kos: {{ $kos->jenis_kos }} </h4>
        <h4>Fasilitas: {{ $kos->fasilitas }} </h4>
        <h4>Harga: {{ $kos->harga }} </h4>
        <h4>Stok kamar: {{ $kos->stok_kamar }} </h4>
    </div>
    <div class="col-md-3">
        <table class="table table-bordered">
            <tr><td><img src="#" width="200" alt=""></td></tr>
            <tr>
                <td>
                    <div class="form-group">
                        <label for="exampleFormControlFile1">Ganti Logo</label>
                        <input type="file" name="logo" class="form-control-file" id="exampleFormControlFile1">
                    </div>
                </td>
            </tr>
        </table>
    </div>
@endsection
