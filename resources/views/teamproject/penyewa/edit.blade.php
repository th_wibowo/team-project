@extends('teamproject.template.adminpanel.adminmaster')

@section('content')
<div class="ml-3 mr-3">
    <div class="card card-primary">
        <div class="card-header">
        <h3 class="card-title">Edit Post {{$penyewa->id}} </h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form action="/penyewa/{{$penyewa->id}}" method="POST">
        @csrf
        @method('PUT')
        <div class="card-body">
            <div class="form-group">
                <label for="name">Nama Penyewa</label>
                <input type="text" class="form-control" id="name" name="name" value=" {{ old('name', $penyewa->name) }}" placeholder="Enter Nama Pemilik">
                @error('name')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="exampleFormControlSelect1">Jenis Kelamin</label>
                <select name="jenis_kelamin" class="form-control" id="exampleFormControlSelect1">
                <option value="L">Laki-Laki</option>
                <option value="P">Perempuan</option>
                </select>
                @error('jenis_kelamin')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="alamat">Alamat</label>
                <input type="text" class="form-control" id="alamat" name="alamat" value=" {{ old('alamat', $penyewa->alamat) }}" placeholder="Enter Alamat">
                @error('alamat')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="email">Email</label>
                <input type="email" class="form-control" id="email" name="email" value=" {{ old('email', $penyewa->email) }}" placeholder="Enter Email">
                @error('email')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="no_hp">No Telpon</label>
                <input type="text" class="form-control" id="no_hp" name="no_hp" value=" {{ old('no_hp', $penyewa->no_hp) }}" placeholder="Enter No Telpon">
                @error('no_rekening')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="npekerjaan">No KTP</label>
                <input type="text" class="form-control" id="no_ktp" name="no_ktp" value=" {{ old('no_ktp', $penyewa->no_ktp) }}" placeholder="Enter No KTP">
                @error('no_rekening')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="pekerjaan">Pekerjaan</label>
                <input type="text" class="form-control" id="pekerjaan" name="pekerjaan" value=" {{ old('pekerjaan', $penyewa->pekerjaan) }}" placeholder="Enter Pekerjaan">
                @error('no_rekening')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <table>
                    <tr><td><img src="#" width="200" alt=""></td></tr>
                    <tr>
                        <td>
                            <div class="form-group">
                                <label for="exampleFormControlFile1">Update Foto</label>
                                <input type="file" name="foto" class="form-control-file" id="foto">
                            </div>
                        </td>
                    </tr>
            </table>
        </div>
        <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Update</button>
        </div>
        </form>
    </div>
</div>
@endsection