@extends('teamproject.template.adminpanel.adminmaster')

@section('content')
    <div class="mt-3 ml-3">
        <h4>Nama penyewa: {{ $penyewa->name }} </h4>
        <h4>Jenis kelamin: {{ $penyewa->jenis_kelamin }} </h4>
        <h4>Alamat: {{ $penyewa->alamat }} </h4>
        <h4>Email: {{ $penyewa->email }} </h4>
        <h4>No. HP: {{ $penyewa->no_hp }} </h4>
        <h4>No. KTP: {{ $penyewa->no_ktp }} </h4>
        <h4>Pekerjaan: {{ $penyewa->pekerjaan }} </h4>
        <h4>Foto: {{ $penyewa->foto }} </h4>
    </div>
@endsection
