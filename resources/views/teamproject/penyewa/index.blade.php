@extends('teamproject.template.adminpanel.adminmaster')

@push('style')
<link rel="stylesheet" href="{{('adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
@endpush

@section('content')
<div class="mt-3 mr-3">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">List Penyewa</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            @if(session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
            <a class="btn btn-primary mb-2" href="{{ route('penyewa.create') }}">Create New Post</a>
            <table id="example1" class="table table-bordered">
            <thead>
                <tr>
                <th style="width: 10px">#</th>
                <th>Nama</th>
                <th>Jenis Kelamin</th>
                <th>Alamat</th>
                <th>Email</th>
                <th>No Telp</th>
                <th>No KTP</th>
                <th>Pekerjaan</th>
                <th style="width: 40px">Actions</th>
                </tr>
            </thead>
            <tbody>
                @forelse($penyewa as $value => $penyewa)
                    <tr>
                        <td> {{ $value + 1 }} </td>
                        <td> {{ $penyewa->name }} </td>
                        <td> {{ $penyewa->jenis_kelamin }} </td>
                        <td> {{ $penyewa->alamat }} </td>
                        <td> {{ $penyewa->email }} </td>
                        <td> {{ $penyewa->no_hp }} </td>
                        <td> {{ $penyewa->no_ktp }} </td>
                        <td> {{ $penyewa->pekerjaan }} </td>
                        <td style="display: flex;" class="justify-content-around"> 
                            <a href="/penyewa/{{$penyewa->id}}" class="btn btn-info btn-sm">show</a>
                            <a href="/penyewa/{{$penyewa->id}}/edit" class="btn btn-default btn-sm">edit</a>
                            {{-- <form action="/penyewa/{{$penyewa->id}}" method="post">
                                @csrf
                                @method('DELETE')
                                <input type="submit" value="delete" class="btn btn-danger btn-sm" onclick="return confirm('Apakah Yakin Data Mau Dihapus')">
                            </form> --}}
                            <button onclick="deleteItem(this)" data-id="{{ $penyewa->id }}" class="btn btn-danger btn-sm">delete</button>
                        </td>
                    </tr>
                    @empty
                        <tr>
                            <td colspan="9" align="center"> No Post</td>
                        </tr>
                @endforelse
            </tbody>
            </table>
        </div>
    </div>    
</div>
@endsection

<script type="application/javascript">

    function deleteItem(e){

        let id = e.getAttribute('data-id');

        const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
                confirmButton: 'btn btn-success',
                cancelButton: 'btn btn-danger'
            },
            buttonsStyling: true
        });

        swalWithBootstrapButtons.fire({
            title: 'Apakah kamu yakin?',
            text: "Data penyewa kos yang sudah dihapus tidak bisa dikembalikan lagi!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya, hapus!',
            cancelButtonText: 'Batal',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                if (result.isConfirmed){

                    $.ajax({
                        type:'DELETE',
                        url:'{{url("/penyewa")}}/' +id,
                        data:{
                            "_token": "{{ csrf_token() }}",
                        },
                        success:function(data) {
                            if (data.success){
                                swalWithBootstrapButtons.fire(
                                    'Deleted!',
                                    'Data penyewa kos terpilih sudah terhapus.',
                                    "success"
                                ).then(function(){
                                    location.reload();
                                });
                            }
                        }
                    });
                }
            } else if (
                result.dismiss === Swal.DismissReason.cancel
            ) {
                swalWithBootstrapButtons.fire(
                    'Cancelled',
                    'Data penyewa kos terpilih masih aman :)',
                    'error'
                );
            }
        });
    }
</script>

@push('scripts')
<script src="{{ asset('adminlte/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{ asset('adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>
@endpush