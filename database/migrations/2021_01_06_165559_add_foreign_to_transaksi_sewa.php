<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignToTransaksiSewa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transaksi_sewa', function (Blueprint $table) {
            $table->unsignedBigInteger('penyewa_id')->nullable();
            $table->foreign('penyewa_id')->references('id')->on('penyewa');

            $table->unsignedBigInteger('kos_id')->nullable();
            $table->foreign('kos_id')->references('id')->on('kos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transaksi_sewa', function (Blueprint $table) {
            $table->dropForeign(['penyewa_id']);
            $table->dropColumn(['penyewa_id']);

            $table->dropForeign(['kos_id']);
            $table->dropColumn(['kos_id']);
        });
    }
}
