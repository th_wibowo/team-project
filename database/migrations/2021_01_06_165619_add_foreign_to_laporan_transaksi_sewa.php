<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignToLaporanTransaksiSewa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('laporan_transaksi_sewa', function (Blueprint $table) {
            $table->unsignedBigInteger('pemilik_id')->nullable();
            $table->foreign('pemilik_id')->references('id')->on('pemilik');

            $table->unsignedBigInteger('transaksi_sewa_id')->nullable();
            $table->foreign('transaksi_sewa_id')->references('id')->on('transaksi_sewa');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('laporan_transaksi_sewa', function (Blueprint $table) {
            $table->dropForeign(['pemilik_id']);
            $table->dropColumn(['pemilik_id']);

            $table->dropForeign(['transaksi_sewa_id']);
            $table->dropColumn(['transaksi_sewa_id']);
        });
    }
}
